def new_keys(dictionary):
    new_dict = {}
    for key in dictionary:
        new_dict[key] = sum(dictionary[key])
    return new_dict


dict_with_keys = {1: [2, 3, 4], 5: [6, 7, 8], 9: [10, 11, 12]}

print(new_keys(dict_with_keys))
