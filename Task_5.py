def book_sorter_by_author(books):
    return sorted(books, key=lambda x: x['author'])


list_of_books = [
    {'title': 'The Great Gatsby', 'author': 'F. Scott Fitzgerald', 'year': 1925},
    {'title': 'To Kill a Mockingbird', 'author': 'Harper Lee', 'year': 1960},
    {'title': '1984', 'author': 'George Orwell', 'year': 1949},
    {'title': 'Pride and Prejudice', 'author': 'Jane Austen', 'year': 1813},
    {'title': 'The Catcher in the Rye', 'author': 'J.D. Salinger', 'year': 1951},
    {'title': 'One Hundred Years of Solitude', 'author': 'Gabriel Garcia Marquez', 'year': 1967},
    {'title': 'The Lord of the Rings', 'author': 'J.R.R. Tolkien', 'year': 1954},
    {'title': 'The Stranger', 'author': 'Albert Camus', 'year': 1942},
    {'title': 'The Hobbit', 'author': 'J.R.R. Tolkien', 'year': 1937},
    {'title': 'The Adventures of Sherlock Holmes', 'author': 'Arthur Conan Doyle', 'year': 1892},
    {'title': 'The Picture of Dorian Gray', 'author': 'Oscar Wilde', 'year': 1890},
    {'title': 'The Da Vinci Code', 'author': 'Dan Brown', 'year': 2003},
]

sorted_books = book_sorter_by_author(list_of_books)

for book in sorted_books:
    print(book)
