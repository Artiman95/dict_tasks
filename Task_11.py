def average_weather_info(weather_dict):
    temperature_sum = 0
    wind_speed_sum = 0
    number_of_days = len(weather_dict)

    for day in weather_dict:
        temperature_sum += weather_dict[day]['temperature']
        wind_speed_sum += weather_dict[day]['wind_speed']

    avg_temperature = temperature_sum / number_of_days
    avg_wind_speed = wind_speed_sum / number_of_days
    return avg_temperature, avg_wind_speed


example_weather_dict1 = {
    '2023-03-28': {'temperature': 15, 'humidity': 60, 'wind_speed': 6},
    '2023-03-29': {'temperature': 18, 'humidity': 50, 'wind_speed': 7},
    '2023-03-30': {'temperature': 20, 'humidity': 55, 'wind_speed': 9},
    '2023-03-31': {'temperature': 19, 'humidity': 54, 'wind_speed': 3},
    '2023-04-01': {'temperature': 21, 'humidity': 57, 'wind_speed': 4}
}
example_weather_dict2 = {
    '2022-03-01': {'temperature': 10, 'humidity': 50, 'wind_speed': 10},
    '2022-03-02': {'temperature': 15, 'humidity': 60, 'wind_speed': 20},
    '2022-03-03': {'temperature': 20, 'humidity': 70, 'wind_speed': 30},
    '2022-03-04': {'temperature': 25, 'humidity': 80, 'wind_speed': 40},
    '2022-03-05': {'temperature': 30, 'humidity': 90, 'wind_speed': 50}
}
print(average_weather_info(example_weather_dict1))
print(average_weather_info(example_weather_dict2))
