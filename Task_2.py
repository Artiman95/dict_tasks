def dict_differences(dict1, dict2):
    new_dict = {}
    for key in dict1:
        if key not in dict2:
            new_dict[key] = dict1[key]
    return new_dict


example_dict1 = {'a': 1, 'b': 2, 'c': 3}
example_dict2 = {'a': 1, 'b': 2, 'd': 5}
# dict_diff = dict_differences(example_dict1, example_dict2)

print(dict_differences(example_dict1, example_dict2))
