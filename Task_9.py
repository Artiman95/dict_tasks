def find_duplicate_keys(dictionary):
    values = list(dictionary.values())
    duplicates = []
    for key in dictionary:
        if values.count(dictionary[key]) > 1:
            duplicates.append(key)
    return duplicates


example_dict1 = {'a': 1, 'b': 2, 'c': 1, 'd': 3, 'e': 2}
example_dict2 = {"dog": 3, "cat": 2, "fish": 1, "bird": 3, "hamster": 1}
print(find_duplicate_keys(example_dict1))
print(find_duplicate_keys(example_dict2))
