def football_table(list_of_teams):
    gols_against_and_for = {}
    for team in list_of_teams:
        team_name = team['team name']
        goals_for = team['goals for']
        goals_against = team['goals against']
        gols_against_and_for[team_name] = goals_for, goals_against
    return gols_against_and_for

        # print('Team', team_name, 'have', goals_for, 'goals for and', goals_against, 'goals against in season 2022/2023')


# Разннобразим задачу и ппросим функцию посчитать разницу забитых/пропущенных как в крутых статистиках
def football_table_difference(list_of_teams):
    for team in list_of_teams:
        team_name = team['team name']
        goals_for = team['goals for']
        goals_against = team['goals against']
        goals_differnce = goals_for - goals_against
        print('Team', team_name, 'have', goals_differnce, 'goals difference in season 2022/2023')


example_list_of_teams = [
    {'team name': 'Manchester United', 'goals for': 41, 'goals against': 35},
    {'team name': 'Arsenal', 'goals for': 66, 'goals against': 26},
    {'team name': 'Manchester City', 'goals for': 67, 'goals against': 25},
    {'team name': 'Tottenham Hotspur', 'goals for': 52, 'goals against': 40},
    {'team name': 'Newcastle United', 'goals for': 39, 'goals against': 19},
    {'team name': 'Liverpool', 'goals for': 47, 'goals against': 29},
    {'team name': 'Brighton & Hove Albion', 'goals for': 46, 'goals against': 31},
    {'team name': 'Brentford', 'goals for': 43, 'goals against': 34},
    {'team name': 'Fulham', 'goals for': 38, 'goals against': 37},
    {'team name': 'Chelsea', 'goals for': 29, 'goals against': 28},
]

print(football_table(example_list_of_teams))
print()
football_table_difference(example_list_of_teams)