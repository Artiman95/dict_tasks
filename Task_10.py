def merge_dicts(dictionary1, dictionary2):
    result = {}
    for key in dictionary1:
        if key in dictionary2:
            result[key] = dictionary1[key]
    return result


example_dict1 = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5}
example_dict2 = {'b': 6, 'e': 7, 'a': 4, 'f': 4, 'g': 5}
print(merge_dicts(example_dict1, example_dict2))
