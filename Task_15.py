def sales_in_different_stores(list_of_stores):
    for store in list_of_stores:
        name = store['store_name']
        product = store['product']
        amount = store['amount']
        price = store['price']
        total_sale = amount * price
        print('Total', product, 'sale in', name, 'is', total_sale)


example_list_of_stores = [
    {'store_name': 'Store A', 'product': 'Product 1', 'amount': 10, 'price': 100},
    {'store_name': 'Store B', 'product': 'Product 2', 'amount': 5, 'price': 50},
    {'store_name': 'Store C', 'product': 'Product 1', 'amount': 15, 'price': 110},
    {'store_name': 'Store D', 'product': 'Product 2', 'amount': 8, 'price': 60},
    {'store_name': 'Supermarket X', 'product': 'Apples', 'amount': 50, 'price': 2.5},
    {'store_name': 'Supermarket Y', 'product': 'Bananas', 'amount': 30, 'price': 1.5},
    {'store_name': 'Supermarket Z', 'product': 'Oranges', 'amount': 20, 'price': 3.0},
    {'store_name': 'Supermarket ZXC', 'product': 'Pears', 'amount': 40, 'price': 2.0},
    {'store_name': 'Electronics Store 1', 'product': 'Laptop', 'amount': 5, 'price': 1000},
    {'store_name': 'Electronics Store 2', 'product': 'Phone', 'amount': 10, 'price': 500},
    {'store_name': 'Furniture Store', 'product': 'Sofa', 'amount': 2, 'price': 1500},
    {'store_name': 'Not Furniture Store', 'product': 'Chair', 'amount': 6, 'price': 200}
]

sales_in_different_stores(example_list_of_stores)
