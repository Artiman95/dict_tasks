def grade_by_subject(list_of_students):
    subject_grades = {}
    for student, grades in list_of_students.items():
        for subject, grade in grades.items():
            if subject in subject_grades:
                subject_grades[subject].append(grade)
            else:
                subject_grades[subject] = [grade]
    return subject_grades


example_list_of_students = {
    'Alice': {'Math': 90, 'English': 80, 'Science': 95},
    'Bob': {'Math': 85, 'English': 75, 'Science': 80},
    'Charlie': {'Math': 75, 'English': 70, 'Science': 85},
    'Tom': {'History': 85, 'English': 75, 'Geography': 80},
    'Jane': {'History': 75, 'English': 80, 'Geography': 90},
    'Harry': {'History': 80, 'English': 85, 'Geography': 75},
    'Emma': {'History': 90, 'English': 90, 'Geography': 85},
    'Peter': {'Physics': 85, 'Chemistry': 75, 'Biology': 80},
    'Sarah': {'Physics': 75, 'Chemistry': 80, 'Biology': 90},
    'John': {'Physics': 80, 'Chemistry': 85, 'Biology': 75},
    'Lucy': {'Physics': 90, 'Chemistry': 90, 'Biology': 85},
    'David': {'Physics': 70, 'Chemistry': 65, 'Biology': 75}
}
print(grade_by_subject(example_list_of_students))
