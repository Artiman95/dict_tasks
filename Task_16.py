def uniq_kv_dictionary(dictionary1, dictionary2):
    new_dictionary = {}
    for key in dictionary1:
        if key not in dictionary2:
            new_dictionary[key] = dictionary1[key]
    for key in dictionary2:
        if key not in dictionary1:
            new_dictionary[key] = dictionary2[key]
    return new_dictionary


example_dict1 = {'a': 1, 'b': 2, 'c': 3, 'd': 5}
example_dict2 = {'a': 4, 'b': 3, 'e': 11, 'f': 7}
print(uniq_kv_dictionary(example_dict1, example_dict2))
