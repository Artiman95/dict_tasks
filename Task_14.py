def count_chars_in_string(string):
    dictionary = {}
    for char in string:
        if char in dictionary:
            dictionary[char] += 1
        else:
            dictionary[char] = 1
    return dictionary


example_string = 'Mary had a little lamborghini'
print(count_chars_in_string(example_string))
