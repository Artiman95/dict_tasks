def sorted_values(d):
    sorted_keys = sorted(d.keys(), reverse=True)
    return [d[key] for key in sorted_keys]


example_dict1 = {'a': 3, 'b': 2, 'c': 7, 'd': 5}
example_dict2 = {3: 'p', 6: 'y', 5: 't', 4: 'h', 1: 'o', 10: 'n'}
print(sorted_values(example_dict1))
print(sorted_values(example_dict2))
