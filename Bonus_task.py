from datetime import datetime, timedelta


def long_trains(train_list):
    for train in train_list:
        travel_time = train['arrival'][1] - train['departure'][1]
        if travel_time >= timedelta(hours=7, minutes=20):
            print(f"Train №{train['train number']} from {train['departure'][0]} в {train['arrival'][0]}")
            print(f"Travel time: {travel_time}")


example_list_of_trains = [
    {"train number": 1, "arrival": ("Москва", datetime(2023, 4, 1, 8, 30)), "departure": ("Санкт-Петербург", datetime(2023, 4, 1, 2, 0))},
    {"train number": 2, "arrival": ("Санкт-Петербург", datetime(2023, 4, 1, 14, 0)), "departure": ("Москва", datetime(2023, 4, 1, 6, 30))},
    {"train number": 3, "arrival": ("Казань", datetime(2023, 4, 1, 12, 0)), "departure": ("Екатеринбург", datetime(2023, 4, 1, 2, 30))},
    {"train number": 4, "arrival": ("Екатеринбург", datetime(2023, 4, 1, 12, 0)), "departure": ("Казань", datetime(2023, 4, 1, 5, 0))},
    {"train number": 5, "arrival": ("Нижний Новгород", datetime(2023, 4, 1, 10, 0)), "departure": ("Краснодар", datetime(2023, 4, 1, 0, 0))},
    {"train number": 6, "arrival": ("Краснодар", datetime(2023, 4, 1, 9, 30)), "departure": ("Нижний Новгород", datetime(2023, 4, 1, 3, 0))},
    {"train number": 7, "arrival": ("Сочи", datetime(2023, 4, 1, 18, 0)), "departure": ("Астрахань", datetime(2023, 4, 1, 8, 0))},
    {"train number": 8, "arrival": ("Астрахань", datetime(2023, 4, 1, 14, 0)), "departure": ("Сочи", datetime(2023, 4, 1, 23, 0))},
    {"train number": 9, "arrival": ("Казань", datetime(2023, 4, 1, 6, 0)), "departure": ("Самара", datetime(2023, 3, 31, 19, 0))},
    {"train number": 10, "arrival": ("Самара", datetime(2023, 4, 1, 10, 0)), "departure": ("Казань", datetime(2023, 4, 1, 3, 0))}
]

long_trains(example_list_of_trains)