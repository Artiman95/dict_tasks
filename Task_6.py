def average_student_score(student_list):
    for student in student_list:
        name = student['name']
        surname = student['surname']
        grades = student['grades']
        average = sum(grades) / len(grades)
        print(name, surname, '- average score:', average)


students = [
    {'name': 'Ivan', 'surname': 'Ivanov', 'grades': [5, 4, 3, 5]},
    {'name': 'Piotr', 'surname': 'Petrov', 'grades': [4, 4, 5, 3]},
    {'name': 'Sidor', 'surname': 'Sidorov', 'grades': [3, 4, 3, 3]},
    {'name': 'Anna', 'surname': 'Banana', 'grades': [5, 5, 4, 5]},
    {'name': 'Nina', 'surname': 'Dobrev', 'grades': [4, 4, 5, 5]},
    {'name': 'Misha', 'surname': 'Mavashi', 'grades': [2, 1, 3, 3]},
    {'name': 'Igor', 'surname': 'Mishin', 'grades': [4, 4, 2, 5]},
    {'name': 'Yana', 'surname': 'Bullet', 'grades': [5, 5, 3, 4]},
    {'name': 'Lena', 'surname': 'Golovach', 'grades': [5, 4, 3, 1]}
]

average_student_score(students)
