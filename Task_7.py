def swap_dict(dictionary):
    return {value: key for key, value in dictionary.items()}


example_dict1 = {'a': 3, 'b': 2, 'c': 7, 'd': 5}
example_dict2 = {3: 'p', 6: 'y', 5: 't', 4: 'h', 1: 'o', 10: 'n'}
print(swap_dict(example_dict1))
print(swap_dict(example_dict2))
