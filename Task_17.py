def avg_price_of_prod(list_of_stores):
    average_costs_in_stores = {}
    for store in list_of_stores:
        name = store['store_name']
        product = store['product']
        amount = store['amount']
        total_cost = store['total_cost']
        avg_cost = total_cost / amount
        average_costs_in_stores[name] = avg_cost
    return average_costs_in_stores


example_list_of_random_stores = [
    {'store_name': 'Store A', 'product': 'Product 1', 'amount': 10, 'total_cost': 1123},
    {'store_name': 'Store B', 'product': 'Product 2', 'amount': 5, 'total_cost': 789},
    {'store_name': 'Store C', 'product': 'Product 1', 'amount': 15, 'total_cost': 12390},
    {'store_name': 'Store D', 'product': 'Product 2', 'amount': 8, 'total_cost': 534},
    {'store_name': 'Supermarket X', 'product': 'Apples', 'amount': 50, 'total_cost': 10213.52},
    {'store_name': 'Supermarket Y', 'product': 'Bananas', 'amount': 30, 'total_cost': 114744.62},
    {'store_name': 'Supermarket Z', 'product': 'Oranges', 'amount': 20, 'total_cost': 45837.0},
    {'store_name': 'Supermarket ZXC', 'product': 'Pears', 'amount': 40, 'total_cost': 78945.0},
    {'store_name': 'Electronics Store 1', 'product': 'Laptop', 'amount': 5, 'total_cost': 12398754},
    {'store_name': 'Electronics Store 2', 'product': 'Phone', 'amount': 10, 'total_cost': 897664},
    {'store_name': 'Furniture Store', 'product': 'Sofa', 'amount': 2, 'total_cost': 87467},
    {'store_name': 'Not Furniture Store', 'product': 'Chair', 'amount': 6, 'total_cost': 465658}
]

print(avg_price_of_prod(example_list_of_random_stores))
