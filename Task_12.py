def create_word_dict(list_of_words):
    word_dict = {}
    for word in list_of_words:
        first_letter = word[0]
        if first_letter not in word_dict:
            word_dict[first_letter] = [word]
        else:
            word_dict[first_letter].append(word)
    return word_dict


example_list_of_words1 = ["apple", "banana", "cherry", "date", "eggplant", "fig", "grape", "honeydew", "iceberg"]
example_list_of_words2 = ["carrot", "broccoli", "zucchini", "kale", "lettuce", "spinach", "onion", "mushroom", "cucumber"]
example_list_of_words3 = ["python", "java", "javascript", "c", "c++", "rust", "swift", "php", "ruby"]
print(create_word_dict(example_list_of_words1))
print(create_word_dict(example_list_of_words2))
print(create_word_dict(example_list_of_words3))
