def max_keys(dictionary):
    max_value = max(dictionary.values())
    for key in dictionary:
        if dictionary[key] == max_value:
            return key


example_dict = {'a': 1, 'b': 2, 'c': 4, 'd': 5}
print(max_keys(example_dict))
