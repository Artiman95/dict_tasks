def find_nested_keys(dictionary):
    nested_keys = []
    new_dict = {}
    for value in dictionary.values():
        if isinstance(value, dict):
            new_dict.update(value)

    for key in dictionary:
        if key in new_dict:
            nested_keys.append(key)
    return nested_keys


example_dict = {
    'a': 1,
    'b': {'c': '2', 'd': 'hello'},
    'c': {'e': 'world', 'f': 'b'},
    'd': {'g': 'a'}
}

print(find_nested_keys(example_dict))
