# variant 1
def new_dictionary(list1, list2):
    new_dict = dict(zip(list1, list2))
    return new_dict


# variant 2
def new_dictionary2(list3, list4):
    new_dict2 = {}
    for i in range(0, len(list3)):
        new_dict2[list3[i]] = list4[i]
    return new_dict2


example_list1 = [1, 2, 3]
example_list2 = ['car', 'banana', 'computer']
print(new_dictionary(example_list1, example_list2))
print(new_dictionary2(example_list1, example_list2))
